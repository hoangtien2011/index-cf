@extends('master')
@section('title')
<section>
    <div class="header-inner-2">
      <div class="inner text-center">
        <h4 class="title text-white uppercase roboto-slab">Menu</h4>
        <h5 class="text-white uppercase">Đặt hàng online</h5>
      </div>
      <div class="overlay bg-opacity-5"></div>
      <img src="/assets_home/images/aboutus_bia1.png" alt="" class="img-responsive"/> </div>
  </section>
@endsection
@section('content1')
    <div class="container">
        <main>
            <div id="sideMenu" class="col-md-2">
                <aside class="sticky" style="margin-top: 20px">
                    <ul>
                        <li style="cursor: pointer;" id="loadAll"><h5><a id="loadAll" class="tab active">Tất cả</h5></li>
                        <li style="cursor: pointer;"><h5><a id="loadCaPhe1" class="tab">Cà Phê</a></h5></li>
                        <li style="cursor: pointer;"><h5><a id="loadTra1" class="tab">Trà</a></h5></li>
                        <li style="cursor: pointer;"><h5><a id="loadSuaChua1" class="tab">Sữa chua</a></h5></li>
                        <li style="cursor: pointer;"><h5><a id="loadKhac1" class="tab">Khác</a></h5></li>
                        <li style="cursor: pointer;"><h5><a id="loadAnChoi1" class="tab">Đồ ăn chơi</a></h5></li>
                    </ul>
                </aside>
            </div>
        {{-- <div class="col-md-1"><hr style="height:1500px; width: 1px; background-color:brown"></div> --}}
        <div id= "menu" class="col-lg-10 menu-hr" style="margin-top:20px">
            <h1 style="color:chocolate; padding:20px">Cà phê phin</h1>
            {{-- cà phê đen --}}
            <div class="row" style="margin-bottom: 20px">
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                    <a href="#">
                  <div class="col-md-10">
                      <figure class="snip1577">
                      <img src="/assets_home/images/menu/capheden.png" alt="sample99" />
                      <figcaption>
                        <h4>Option: nóng/đá</h4>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Cà phê đen</h4></div>
                        <div class="text-nowrap"><h5>19.000 VNĐ</h5></div>
                  </div>
                </a>
                </div>
                {{-- nâu --}}
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng" >
                    <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/caphenau.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Cà phê nâu</h4></div>
                        <div class="text-nowrap"><h5>25.000 VNĐ</h5></div>
                    </div>
                    </a>
                </div>
                {{-- sữa đá sg --}}
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                    <a href="#">
                        <div class="col-md-10">
                            <figure class="snip1577">
                                <img src="/assets_home/images/menu/suadasaigon.png" alt="sample99" />
                                <figcaption>
                                  <h4>Option: Đá</h4>
                                </figcaption>
                              </figure>
                        </div>
                        <div class="col-md-3" style="margin-left:10px;">
                            <div class="text-nowrap"><h4>Sữa đá Sài Gòn</h4></div>
                            <div class="text-nowrap"><h5>29.000 VNĐ</h5></div>
                        </div>
                        </a>
                </div>
            </div>
            {{-- row 2 --}}
            <div class="row" style="margin-bottom: 20px">
                {{-- cf tứng --}}
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/caphetrung.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Cà phê trứng</h4></div>
                        <div class="text-nowrap"><h5>35.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
              {{-- cf múi --}}
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/caphemuoi.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Cà phê muối</h4></div>
                        <div class="text-nowrap"><h5>29.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
              {{-- cf sữa tươi --}}
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/caphe sua tuoi.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Cà phê sữa tươi</h4></div>
                        <div class="text-nowrap"><h5>25.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
            </div>
            {{-- row 3 --}}
            <div class="row" style="margin-bottom: 20px">
                {{-- cf dừa --}}
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                    <a href="#">
                        <div class="col-md-10">
                            <figure class="snip1577">
                                <img src="/assets_home/images/menu/caphedua.png" alt="sample99" />
                                <figcaption>
                                <h4>Option: đá</h4>
                                </figcaption>
                            </figure>
                        </div>
                        <div class="col-md-3" style="margin-left:10px;">
                            <div class="text-nowrap"><h4>Cà phê Dừa</h4></div>
                            <div class="text-nowrap"><h5>40.000 VNĐ</h5></div>
                        </div>
                    </a>
                </div>
            </div>
            {{-- row 4 CF máy --}}
            <h1 style="color:chocolate; margin-left: 20px;margin-bottom:20px">Cà phê máy</h1>
            <div class="row" style="margin-bottom: 20px">
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/espresso.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Espresso</h4></div>
                        <div class="text-nowrap"><h5>35.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>

              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/latte.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Latte</h4></div>
                        <div class="text-nowrap"><h5>39.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>

              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/capuchino.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Capuchino</h4></div>
                        <div class="text-nowrap"><h5>39.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/americano.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Americano</h4></div>
                        <div class="text-nowrap"><h5>25.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
            </div>
            <h1 style="color:chocolate; padding:20px">Trà</h1>
            {{-- Trà page --}}
            <div class="row" style="margin-bottom: 20px">
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                    <a href="#">
                  <div class="col-md-10">
                      <figure class="snip1577">
                      <img src="/assets_home/images/menu/tralipton.png" alt="sample99" />
                      <figcaption>
                        <h4>Option: nóng/đá</h4>
                      </figcaption>
                    </figure>
                  </div>
                  <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Trà lipron</h4></div>
                        <div class="text-nowrap"><h5>25.000 VNĐ</h5></div>
                  </div>
                </a>
                </div>
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng" >
                    <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/tracamsamatong.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Trà cam sả mật ong</h4></div>
                        <div class="text-nowrap"><h5>50.000 VNĐ</h5></div>
                    </div>
                    </a>
                </div>
                <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                    <a href="#">
                        <div class="col-md-10">
                            <figure class="snip1577">
                                <img src="/assets_home/images/menu/tradao.png" alt="sample99" />
                                <figcaption>
                                  <h4>Option: Đá</h4>
                                </figcaption>
                              </figure>
                        </div>
                        <div class="col-md-3" style="margin-left:10px;">
                            <div class="text-nowrap"><h4>Trà đào</h4></div>
                            <div class="text-nowrap"><h5>35.000 VNĐ</h5></div>
                        </div>
                        </a>
                </div>
            </div>
            <div class="row" style="margin-bottom: 20px">
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/traxanhchanhday.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: nóng/đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Trà xanh chanh dây</h4></div>
                        <div class="text-nowrap"><h5>35.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
              <div class="col-md-4" data-bs-toggle="tooltip" data-bs-placement="right" title="Đặt hàng">
                <a href="#">
                    <div class="col-md-10">
                        <figure class="snip1577">
                            <img src="/assets_home/images/menu/tranhietdoi.png" alt="sample99" />
                            <figcaption>
                              <h4>Option: Đá</h4>
                            </figcaption>
                          </figure>
                    </div>
                    <div class="col-md-3" style="margin-left:10px;">
                        <div class="text-nowrap"><h4>Trà nhiệt đới</h4></div>
                        <div class="text-nowrap"><h5>40.000 VNĐ</h5></div>
                    </div>
                    </a>
              </div>
            </div>
            {{-- Sữa chua page --}}
            <h1 style="color:chocolate; padding:20px">Sữa chua </h1>



        </div>
    </main>
</div>

@endsection

