@extends('master')
@section('title')
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header bg-success">
          <h5 class="modal-title" id="exampleModalLabel">Đóng góp ý kiến thành công</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" aria-hidden="true">
              <span>&times;</span>
          </button>
        </div>
        <div class="modal-body text-success">
         Cảm ơn bạn đã đóng góp ý kiến cho cửa hàng <i class="fas fa-heart text-red"></i>
        </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('content1')
<section>
    <div class="header-inner-2">
      <div class="inner text-center">
        <h4 class="title text-white uppercase roboto-slab">Liên hệ với chúng tôi</h4>
        <h5 class="text-white uppercase ">Bạn muốn cứ góp ý chúng tôi sẽ lắng nghe</h5>
      </div>
      <div class="overlay bg-opacity-5"></div>
      <img src="/assets_home/images/aboutus_bia1.png" alt="" class="img-responsive"/> </div>
  </section>
@endsection
@section('content2')
<section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <div class="iconbox-xtiny center"><img src="/assets_home/images/324.png"></div>
          <h6 class="uppercase text-brown nopadding">Kết nối</h6>
          <h3 class="uppercase roboto-slab paddtop1">Giữa của hàng và khách hàng</h3>
          <div class="title-line-4 brown less-margin align-center"></div>
        </div>
        <div class="clearfix"></div>

      <div class="col-md-8 border">


        <div class="smart-forms bmargin">
            <h3 class="raleway text-brown">Hòm thư đóng góp ý kiến <i class="fas fa-inbox text-brown"></i></h3>
            <br/>
            <br/>
            <form method="post" action="php/smartprocess.php" id="smart-form">
                {{-- smart process xử lý sâu quá quay lại học sau :v --}}
              <div>
                <div class="section">
                  <label class="field prepend-icon">
                    <input type="text" name="sendername" id="sendername" class="gui-input" placeholder="Nhập tên nè">
                    <span class="field-icon"><i class="fa fa-user"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <input type="email" name="emailaddress" id="emailaddress" class="gui-input" placeholder="Rồi nhập Email nè">
                    <span class="field-icon"><i class="fa fa-envelope"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section colm colm6">
                  <label class="field prepend-icon">
                    <input type="tel" name="telephone" id="telephone" class="gui-input" placeholder="Rồi nhập số điện thoại nè">
                    <span class="field-icon"><i class="fa fa-phone-square"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <input type="text" name="sendersubject" id="sendersubject" class="gui-input" placeholder="Lý do bạn phải đến trang này">
                    <span class="field-icon"><i class="fa fa-lightbulb-o"></i></span> </label>
                </div>
                <!-- end section -->

                <div class="section">
                  <label class="field prepend-icon">
                    <textarea class="gui-textarea" id="sendermessage" name="sendermessage" placeholder="Trình bày đi nè =))))"></textarea>
                    <span class="field-icon"><i class="fa fa-comments"></i></span> <span class="input-hint"> <strong>Hint:</strong> Nhập ít nhất 20 ký tự nhé</span> </label>
                </div>
                <!-- end section -->
                <!--<div class="section">
                            <div class="smart-widget sm-left sml-120">
                                <label class="field">
                                    <input type="text" name="captcha" id="captcha" class="gui-input sfcode" maxlength="6" placeholder="Enter CAPTCHA">
                                </label>
                                <label class="button captcode">
                                    <img src="php/captcha/captcha.php?<?php echo time();?>" id="captchax" alt="captcha">
                                    <span class="refresh-captcha"><i class="fa fa-refresh"></i></span>
                                </label>
                            </div>
                        </div>-->

                <div class="result"></div>
                <!-- end .result  section -->

              </div>
              <!-- end .form-body section -->
              <div class="form-footer">
                {{-- <button type="submit" data-btntext-sending="Sending..." class="button btn-primary brown">Submit</button> --}}
                <button type="submit" data-btntext-sending="Sending..." class="button btn-primary brown" data-toggle="modal" data-target="#exampleModal">
                    Gửi
                  </button>
                <button type="reset" class="button"> Hủy </button>
              </div>
              <!-- end .form-footer section -->
            </form>
          </div>
          <!-- end .smart-forms section -->
      </div>


      <div class="col-md-4 bg-info border border-success">
      <br/>
          <h4 class="roboto-slab uppercase ">Địa chỉ cửa hàng</h4>
            166/6 Nguyễn Công Trứ, Sơn Trà, Đà Nẵng <br />
            Telephone: +84 929 305 691<br />
            <br />
            E-mail: <a href="mailto:hoangtien0538@gmail.com">steaminmugs@gmail.com</a><br />
            Website: <a href="/">www.steaminmugs.com</a>
            <div class="clearfix"></div>
		<br/><br/>
        <h4 class=" roboto-slab uppercase">Địa chỉ trên map</h4>

       <div id="map" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3833.9621531515054!2d108.23113301416986!3d16.067453643754938!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3142182bc1dc1e3b%3A0xd46d9f785ffcb842!2zMTY2IE5ndXnhu4VuIEPDtG5nIFRy4bupLCBBbiBI4bqjaSBC4bqvYywgU8ahbiBUcsOgLCDEkMOgIE7hurVuZyA1NTAwMDAsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1637681231124!5m2!1svi!2s" width="360" height="250" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</div>

        </div>
      </div>
    </div>
  </section>
@endsection
