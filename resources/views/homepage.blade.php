@extends('master')
@section('title')
<div class="master-slider margintop-3 ms-skin-default" id="masterslider">
        <div class="ms-slide slide-1" data-delay="9">
            <div class="slide-pattern"></div>
            <img src="/assets_home/js/masterslider/blank.gif" data-src="/assets_home/images/bia1.png" alt="" />
            <img src="/assets_home/js/masterslider/blank.gif" data-src="/assets_home/images/sliders/masterslider/Steamin' mug Coffee.png" alt="" style="left: 650px; top: 160px; margin-top:150px" class="ms-layer" data-type="image" data-delay="500" data-duration="2500" data-ease="easeOutExpo" data-effect="scalefrom(1.1,1.1,30,0)" />
            <a class="ms-layer sbut1" style="left: 670px; top: 570px; " data-type="text" data-delay="1500" data-ease="easeOutExpo" data-duration="1200" data-effect="scale(1.5,1.6)"> Đặt bàn </a>
            <a class="ms-layer sbut10" href="/menu-cafe" style="left: 830px; top: 570px;" data-type="text" data-delay="2000" data-ease="easeOutExpo" data-duration="1200" data-effect="scale(1.5,1.6)"> Đặt hàng </a>
        </div>
        <!-- end slide 1 -->
        <!-- slide 2 -->
        <div class="ms-slide slide-2" data-delay="9">
            <div class="slide-pattern"></div>
            <img src="/assets_home/js/masterslider/blank.gif" data-src="/assets_home/images/bia2.png" alt="" />
        </div>
        <!-- end slide 2 -->
        <!-- slide 3 -->
        <div class="ms-slide slide-3" data-delay="9">
            <div class="slide-pattern"></div>
            <img src="/assets_home/js/masterslider/blank.gif" data-src="/assets_home/images/bia3.png" alt="" />
        </div>
        <!-- end slide 3 -->
</div>
@endsection

@section('content1')
<section class=" sec-dummy-top-padding">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
            <div class="iconbox-xtiny center">
            <img src="/assets_home/images/324.png" alt="" />
            </div>
            <h6 class="uppercase text-brown nopadding"></h6>
            <h3 class="uppercase roboto-slab paddtop1">Welcome to Steamin' Mugs Coffee</h3>
            <div class="title-line-4 brown less-margin align-center"></div>
            <p class="sub-title-2">Một tách cà phê uống vào buổi sáng mang lại sự hưng phấn tuyệt vời mà không một tách cà phê nào khác dù là buổi chiều hay buổi tối có thể tạo ra được.</p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection

@section('content2')
<section class="sec-bpadding-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 bmargin nopadding ">
            <img src="/assets_home/images/beans.png" alt="" class="img-responsive" />
          </div>
          <!--end item-->
          <div class="col-md-6 bmargin padding-left-5">
            <div class="item-holder">
              <h3 class=" text-brown"> Các loại hạt cà phê </h3>
              <p>
              <h4 class="text-brown">
                <i class="fas fa-angle-right"></i> Hạt cà phê Culi
              </h4>
              <br /> Là những hạt cà phê có hình dạng tròn. Điều đặc biệt là trong một trái chỉ có duy nhất một hạt. Tuy có vị đắng gắt nhưng hương thơm thì vô cùng say đắm. </p>
              <br />
              <p>
              <h4 class="text-brown">
                <i class="fas fa-angle-right"></i> Hạt cà phê Arabica
              </h4>
              <br />Có vị chua thanh xen lẫn với vị đắng nhẹ, mùi hương rất thanh tao, quý phái, nước pha màu nước nâu nhạt, trong trẻo của hổ phách. Hạt cà phê Arabica không chỉ là nguyên liệu chính của cà phê Trung Nguyên mà còn của các thương hiệu cà phê nổi tiếng nhất trên thế giới. </p>
              <br />
              <p>
              <h4 class="text-brown">
                <i class="fas fa-angle-right"></i> Hạt cà phê Robusta
              </h4>
              <br />Trải qua quá trình chế biến trên dây chuyền thiết bị hiện đại với công nghệ cao đã tạo ra loại cà phê Robusta có mùi thơm dịu, vị đắng gắt, nước có màu nâu sánh, không chua, hàm lượng cafein vừa đủ đã tạo nên một loại cà phê đặc sắc phù hợp với khẩu vị của người dân Việt Nam. </p>
              <br />
              <br />
              <a class="btn btn-border brown">Đọc thêm</a>
            </div>
          </div>
        </div>
        <!--end item-->
      </div>
    </div>
  </section>
@endsection
@section('content3')
<div id="section-3">
    <section class="parallax-section31">
      <div class="section-overlay bg-opacity-4">
        <div class="container sec-tpadding-3 sec-bpadding-3">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-xs-12 text-left">
                      <div>
                        <img src="/assets_home/images/327.png" alt="" />
                      </div>
                      <h6 class="uppercase nopadding text-white">Đặt hàng online</h6>
                      <h3 class="uppercase roboto-slab paddtop1 text-white">Menu đặc biệt</h3>
                      <div class="title-line-4 white align-left less-margin"></div>
                      <div class="clearfix"></div>
                      <p class="text-white">Discount 10% khi order qua webiste</p>
                      <br />
                      <a class="btn btn-border white brown">Xem thêm</a>
                      <a class="btn btn-brown">Order Ngay</a>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <!--end item-->
                  <div class="col-md-6 bmargin">
                    <div class="special-menu-holder bmargin pull-right">
                      <div class="text-box">
                        <ul class="price-list">
                          <li>
                            <span class="item">
                              <h5 class="roboto-slab font-weight-3">Cà phê đen</h5>
                            </span>
                            <span class="line">------------</span>
                            <span class="price">
                              <h5 class="text-brown">20.000 VND</h5>
                            </span>
                          </li>
                          <li>
                            <span class="item">
                              <h5 class="roboto-slab font-weight-3">Cà phê nâu </h5>
                            </span>
                            <span class="line">------------</span>
                            <span class="price">
                              <h5 class="text-brown">25.000 VND</h5>
                            </span>
                          </li>
                          <li>
                            <span class="item">
                              <h5 class="roboto-slab font-weight-3">Sữa đá Sài Gòn</h5>
                            </span>
                            <span class="line">------------</span>
                            <span class="price">
                              <h5 class="text-brown">29.000 VND</h5>
                            </span>
                          </li>
                          <li>
                            <span class="item">
                              <h5 class="roboto-slab font-weight-3">Cà phê sữa tươi</h5>
                            </span>
                            <span class="line">------------</span>
                            <span class="price">
                              <h5 class="text-brown">25.000 VND</h5>
                            </span>
                          </li>
                          <li>
                            <span class="item">
                              <h5 class="roboto-slab font-weight-3">Cà phê dừa</h5>
                            </span>
                            <span class="line">------------</span>
                            <span class="price">
                              <h5 class="text-brown">40.000 VND</h5>
                            </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <!--end item-->
            </div>
        </div>
      </div>
    </section>
  </div>
  <!--end section-->
@endsection
@section('content4')
<div id="section-4">
    <section class="sec-padding">
      <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="iconbox-xtiny center">
                  <img src="/assets_home/images/324.png" alt="" />
                </div>
                <h6 class="uppercase text-brown nopadding">Những loại cà phê</h6>
                <h3 class="uppercase roboto-slab paddtop1">Được ưa chuộng</h3>
                <div class="title-line-4 brown less-margin align-center"></div>
                <p class="sub-title-2">Bốn loại cà phê ngon và được ưa chuộng</p>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-3 text-center">
                <div class="feature-box48 bmargin">
                  <div class="icon"></div>
                  <h5 class="uppercase roboto-slab font-weight-4">Cà phê nâu</h5>
                  <p>Thức uống không thể chối từ của người Hà Nội</p>
                </div>
              </div>
              <!--end item-->
              <div class="col-md-3 text-center">
                <div class="feature-box48 bmargin">
                  <div class="icon two"></div>
                  <h5 class="uppercase roboto-slab font-weight-4">Cà phê đen</h5>
                  <p>Mạnh mẽ, đậm chất trưởng thành</p>
                </div>
              </div>
              <!--end item-->
              <div class="col-md-3 text-center">
                <div class="feature-box48 bmargin">
                  <div class="icon three"></div>
                  <h5 class="uppercase roboto-slab font-weight-4">Sữa đá Sài Gòn</h5>
                  <p>Ngon, bổ, rẻ, nạp năng lượng cho cả ngày làm việc</p>
                </div>
              </div>
              <!--end item-->
              <div class="col-md-3 text-center">
                <div class="feature-box48 bmargin">
                  <div class="icon four"></div>
                  <h5 class="uppercase roboto-slab font-weight-4">Cà phê dừa</h5>
                  <p>Dễ uống, không quá nhiều cà phê phù hợp cho mọi lứa tuổi</p>
                </div>
              </div>
              <!--end item-->

        </div>
      </div>
    </section>
  </div>
  <!--end section-->
@endsection
@section('content5')
<div id="section-5">
    <section class="parallax-section32">
      <div class="section-overlay bg-opacity-4">
        <div class="container sec-tpadding-2 sec-bpadding-2">
          <div class="row">
            <div class="col-xs-12 text-center">
                <div class="iconbox-xtiny center">
                  <img src="/assets_home/images/327.png" alt="" />
                </div>
                <h6 class="uppercase nopadding text-white">Feedback</h6>
                <h3 class="uppercase roboto-slab paddtop1 text-white">Từ những khách hàng thân yêu</h3>
                <div class="title-line-4 white less-margin align-center"></div>
                <p class="sub-title-2 text-white"></p>
              </div>
              <div class="clearfix"></div>
              <div class="col-md-4">
                <div class="blog-holder11 bmargin">
                  <div class="image-holder">
                    <a href="#">
                      <img src="/assets_home/images/feedback1.png" alt="" class="img-responsive" />
                    </a>
                  </div>
                  <div class="text-box-inner text-left">
                    <h4 class="roboto-slab font-weight-3 less-mar3 uppercase">
                      <a href="#">Cà phê đen</a>
                    </h4>
                    <div class="blog-post-info">
                      <span>
                        <i class="fa fa-user"></i> Từ 1120_neit </span>
                      <span>
                        <i class="fa fa-comments-o"></i> 2011 Comments </span>
                    </div>
                    <br />
                    <p>Thêm chút đường cà phê có ngọt? Thêm chút tình mình có thuộc về nhau?</p>
                  </div>
                </div>
              </div>
              <!--end item-->
              <div class="col-md-4">
                <div class="blog-holder11 bmargin">
                  <div class="image-holder">
                    <a href="#">
                      <img src="/assets_home/images/feedback2.png" alt="" class="img-responsive" />
                    </a>
                  </div>
                  <div class="text-box-inner text-left">
                    <h4 class="roboto-slab font-weight-3 less-mar3 uppercase">
                      <a href="#">Cà phê nâu</a>
                    </h4>
                    <div class="blog-post-info">
                      <span>
                        <i class="fa fa-user"></i> Từ haz.oe </span>
                      <span>
                        <i class="fa fa-comments-o"></i> 1811 Comments </span>
                    </div>
                    <br />
                    <p>Lúc gặp gỡ, tán tỉnh trai gái lấy cafe để làm cái cớ gặp gỡ. Lúc yêu đương, họ lại lấy cafe như một cái cớ tìm điểm dừng chân, chỉ cần ngồi bên nhau, nói vài 3 câu chuyện cũng thành một buổi hẹn hò vui vẻ bất chấp hè hay đông.</p>
                  </div>
                </div>
              </div>
              <!--end item-->
              <div class="col-md-4">
                <div class="blog-holder11 bmargin">
                  <div class="image-holder">
                    <a href="#">
                      <img src="/assets_home/images/feedback3.png" alt="" class="img-responsive" />
                    </a>
                  </div>
                  <div class="text-box-inner text-left">
                    <h4 class="roboto-slab font-weight-3 less-mar3 uppercase">
                      <a href="#">Cà phê dừa</a>
                    </h4>
                    <div class="blog-post-info">
                      <span>
                        <i class="fa fa-user"></i> Từ l_o_n_l_yyyy </span>
                      <span>
                        <i class="fa fa-comments-o"></i> 212 Comments </span>
                    </div>
                    <br />
                    <p>Cứ mỗi lần muốn sống chậm lại, tôi đều tìm đến cà phê. Mùi hương của nó len lỏi vào đầu óc, khiến tôi như quay cuồng, ma mị.</p>
                  </div>
                </div>
              </div>
              <!--end item-->
          </div>
        </div>
      </div>
    </section>
  </div>
  <!--end section-->
@endsection
@section('content6')
<div id="section-6">
    <section>
      <div class="beans-bg-holder">
        <div class="container sec-tpadding-2 sec-bpadding-2">
          <div class="row">
            <div class="col-md-6 bmargin">
                <div class="col-xs-12 text-center">
                  <div class="iconbox-xtiny center">
                    <img src="/assets_home/images/324.png" alt="" />
                  </div>
                  <h6 class="uppercase text-brown nopadding">Những ý kiến đóng góp</h6>
                  <h4 class="uppercase roboto-slab paddtop1">Người ta nói</h4>
                  <div class="title-line-4 brown less-margin align-center"></div>
                </div>
                <div class="clearfix"></div>
                <br />
                <div id="owl-demo11" class="owl-carousel">
                  <div class="item">
                    <div class="iconbox-medium left round overflow-hidden">
                      <img src="/assets_home/images/ava1.png" alt="" class="img-responsive" />
                    </div>
                    <div class="text-box-right more-padding-2 text-white">
                      <p>Đây là quán cà phê của bạn mình, mình rất bất ngờ khi nó có thể mở được một quán cà phê như thế này, cà phê vừa thơm, vừa ngon, không quá nặng nên mình rất thích uống</p>
                      <div class="divider-line light solid margin"></div>
                      <h6 class="roboto-slab less-mar1">Nguyễn Kim Kiều Loan</h6>
                      <span class="text-brown">Staff</span>
                    </div>
                  </div>
                  <!--end slide item-->
                  <div class="item">
                    <div class="iconbox-medium left round overflow-hidden">
                      <img src="/assets_home/images/ava3.png" alt="" class="img-responsive" />
                    </div>
                    <div class="text-box-right more-padding-2 text-white">
                      <p>Khi tình yêu chớm nở, tình yêu đẹp và lãng mạn. Đó là sự háo hức đến run rẩy con tim, là sự rung động mong ngóng, chờ đợi, là sự tươi mới đầy vui vẻ. Nó ngọt ngào, dịu nhẹ, dễ chịu trong từng khoảnh khắc giống như tách capuchino. Capuchino nhiều bọt, ngọt nhẹ hòa quyện trọn vẹn giữa hương vị đặc trưng của cafe với ngọt, thơm của sữa. Capuchino dễ uống mang đến cho người ta cảm giác nhẹ nhàng đẹp mắt với những tách cafe capuchino được vẽ bằng bọt sữa độc đáo, ấn tượng. Trong một ly capuchino, phái nam là cafe và phái nữ là bọt sữa. Hai thứ hòa quyện vào nhau tựa như trái tim yêu đương nhiệt huyết.</p>
                      <div class="divider-line light solid margin"></div>
                      <h6 class="roboto-slab less-mar1">Đặng Huyền Trang</h6>
                      <span class="text-brown">Khách Hàng</span>
                    </div>
                  </div>
                  <!--end slide item-->
                  <div class="item">
                    <div class="iconbox-medium left round overflow-hidden">
                      <img src="/assets_home/images/ava4.png" alt="" class="img-responsive" />
                    </div>
                    <div class="text-box-right more-padding-2 text-white">
                      <p>Ta tìm gì trong một tách cà phê: một chút tỉnh táo, một chút lãng du nghênh ngang ngồi lại bất động giữa phố thị cứ vồn vã trôi đi, tìm giây phút lặng yên cạnh ai đó, cái thở dài trước ngày cứ trôi qua hay một nỗi nhớ ngọt đắng vơi đầy.</p>
                      <div class="divider-line light solid margin"></div>
                      <h6 class="roboto-slab less-mar1">Nguyễn Đức Tùng</h6>
                      <span class="text-brown">Khách Hàng</span>
                    </div>
                  </div>
                  <!--end slide item-->
                  <div class="item">
                    <div class="iconbox-medium left round overflow-hidden">
                      <img src="/assets_home/images/ava2.png" alt="" class="img-responsive" />
                    </div>
                    <div class="text-box-right more-padding-2 text-white">
                      <p>Cà phê là loại thức uống đặc biệt với vị đắng đầu lưỡi, sau đó là vị ngọt nơi cuống họng. Vị của cà phê cũng giống như cuộc đời con người, trải qua nhiều lần vấp ngã sẽ đứng lên mạnh mẽ hơn trước. Cuộc sống với những bộn bề mọi lo toan, suy tính cuốn con người theo một cách vội vã. Hãy dành ra một chút thời gian ngồi ngắm nhìn đường phố và nhâm nhi một tách cà phê để ngẫm về người, về đời và ngẫm lại mình.</p>
                      <div class="divider-line light solid margin"></div>
                      <h6 class="roboto-slab less-mar1">Hoàng Việt Tiến</h6>
                      <span class="text-brown">Cafe Manager</span>
                    </div>
                  </div>
                  <!--end slide item-->
                </div>
                <!--end carousel-->
              </div>
              <!--end item-->
              <div class="col-md-6 bmargin">
                <div class="col-xs-12 text-center">
                  <div class="iconbox-xtiny center">
                    <img src="/assets_home/images/324.png" alt="" />
                  </div>
                  <h6 class="uppercase text-brown nopadding">Steamin' Mugs Coffee</h6>
                  <h4 class="uppercase roboto-slab paddtop1">Cơ sở vật chất</h4>
                  <div class="title-line-4 brown less-margin align-center"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client1.png" alt="" />
                </div>
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client2.png" alt="" />
                </div>
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client3.png" alt="" />
                </div>
                <div class="clearfix"></div>
                <br />
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client4.png" alt="" />
                </div>
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client5.png" alt="" />
                </div>
                <div class="col-md-4 col-sm-4 nopadding">
                  <img src="/assets_home/images/client6.png" alt="" />
                </div>
              </div>
              <!--end item-->
          </div>
        </div>
      </div>
    </section>
  </div>
@endsection
