@extends('master')
@section('title')
<section>
    <div class="header-inner-2">
      <div class="inner text-center">
        <h4 class="title text-white uppercase roboto-slab">About Us</h4>
        <h5 class="text-white uppercase">Câu chuyện đằng sau Steamin' Mugs</h5>
      </div>
      <div class="overlay bg-opacity-5"></div>
      <img src="/assets_home/images/aboutus_bia1.png" alt="" class="img-responsive"/>
    </div>
  </section>
@endsection
@section('content2')
<section class=" sec-tpadding-2">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <div class="iconbox-xtiny center"><img src="/assets_home/images/324.png" alt=""/></div>
          <h6 class="uppercase text-brown nopadding">Chúng tôi là ai?</h6>
          <h3 class="uppercase roboto-slab paddtop1">Lịch sử hình thành Steamin' Mugs</h3>
          <div class="title-line-4 brown less-margin align-center"></div>
          <p class="sub-title-2"></p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </section>
@endsection
@section('content3')
<section class="sec-bpadding-2">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-6 bmargin nopadding "> <img src="/assets_home/images/about us story.png" alt="" class="img-responsive"/> </div>
        <!--end item-->

        <div class="col-md-6 bmargin padding-left-5">
          <div class="item-holder">
            <h4 class=" text-brown">Câu chuyện đằng sau cái tên Steamin' Mugs</h4>
            <p>Vào một ngày thời tiết se lạnh vào tháng 11, nhóm chúng tôi hẹn nhau đi cà phê, hôm đó là ngày khai trương của quán nên quán có tổ chức bốc thăm cho tham quan quá trình sản xuất 1 ly cà phê trong quầy bar như thế nào. Tôi cũng chỉ order nước và ra bàn như mọi người thế nhưng bạn nhân viên đến nói với tôi là tôi là người may mắn được trúng chuyên tham quan đó.</p>
            <br/>
            <p>Tôi được chứng kiến tận mắt từ quá trình đóng gói cà về hạt trong phòng kín, đến quá trình xay cà phê và cho vào máy để có thể cho ra một ly cà phê nóng với những làn khói bay nghi ngút. Mùi thơm của ly cà phê đó khiến tôi không thể nào từ chối nó được.</p>
            <br/>
            <p>Kết thúc chuyến đi tham quan đó thì tôi lại nung nấu ý tưởng mở 1 tiệm cà phê như vậy với cái tên Steamin' Mugs với sứ mệnh đưa những ly cà phê tuyệt vời nhất đến với người Việt Nam. Tôi đã tìm đến những người bạn của mình để chung vốn đầu tư và thành lập thương hiệu Steamin' Mugs và gầy dựng lên đến tận bây giờ.</p>
            <br/>
            <br/>
            <a class="btn btn-border brown">Xem thêm</a></div>
        </div>
        <!--end item-->
      </div>
    </div>
  </section>
@endsection
@section('content4')
<section class="sec-padding">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 text-center">
          <div class="iconbox-xtiny center"><img src="/assets_home/images/324.png" alt=""/></div>
          <h6 class="uppercase text-brown nopadding">Chúng tôi có những ai?</h6>
          <h3 class="uppercase roboto-slab paddtop1">Gặp gỡ đội ngũ của chúng tôi</h3>
          <div class="title-line-4 brown less-margin align-center"></div>
          <p class="sub-title-2"></p>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4 col-sm-6">
          <div class="team-holder7 two bmargin">
            <div class="team-member"><img src="/assets_home/images/aboutus_ceo.png" alt="" class="img-responsive"/></div>
            <div class="info-box text-center">
              <h5 class="uppercase roboto-slab less-mar2">Nguyễn Đức Thịnh</h5>
              <span class="text-brown">Founder and CEO</span> <br/>
              <br/>
              <p>Tạo ra một thương hiệu cà phê nổi tiếng và đem lại những ly cà phê chất lượng nhất là trách nhiệm của tôi</p>
              <br/>
              <ul class="social-icons">
                <li><a href="https://www.facebook.com/ndtbon" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/ducthinhndt" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://solo.to/ndtbon" target="_blank"><i class="fas fa-infinity"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--end item-->

        <div class="col-md-4 col-sm-6">
          <div class="team-holder7 two bmargin">
            <div class="team-member"><img src="/assets_home/images/aboutus_manager.png" alt="" class="img-responsive"/></div>
            <div class="info-box text-center">
              <h5 class="uppercase roboto-slab less-mar2">Hoàng Việt Tiến</h5>
              <span class="text-brown">Cafe Manager</span> <br/>
              <br/>
              <p>Chất lượng của những hạt cà phê phải tốt thì mới có thể đưa ra những ly cà phê tốt nhất đến khách hàng được.</p>
              <br/>
              <ul class="social-icons">
                <li><a href="https://www.facebook.com/hoangviettien2011" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/1120_neit" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="https://tienhoang2011.bio.link" target="_blank"><i class="fas fa-infinity"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--end item-->

        <div class="col-md-4 col-sm-6">
          <div class="team-holder7 two bmargin">
            <div class="team-member"><img src="/assets_home/images/aboutus_staff.png" alt="" class="img-responsive"/></div>
            <div class="info-box text-center">
              <h5 class="uppercase roboto-slab less-mar2">Nguyễn Kim Kiều Loan</h5>
              <span class="text-brown">Staff</span> <br/>
              <br/>
              <p>Phục vụ với 100% trái tim để đem lại trải nghiệm tốt nhất cho khách hàng.</p>
              <br/>
              <ul class="social-icons">
                <li><a href="https://www.facebook.com/kieuloan.sociu.212" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/l_o_n_l_yyy" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#" target="_blank"><i class="fas fa-infinity"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--end item-->
      </div>
    </div>
  </section>
@endsection
