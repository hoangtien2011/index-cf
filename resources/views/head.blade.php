<link href="/assets_home/images/logo.ico" rel="shortcut icon" type="image/x-icon" />
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link href='https://www.google.com/fonts#UsePlace:use/Collection:Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://www.google.com/fonts#UsePlace:use/Collection:Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Stylesheets -->
<link rel="stylesheet" media="screen" href="/assets_home/js/bootstrap/bootstrap.min.css" type="text/css" />
<link rel="stylesheet" href="/assets_home/js/mainmenu/menu.css" type="text/css" />
<link rel="stylesheet" href="/assets_home/css/default.css" type="text/css" />
<link rel="stylesheet" href="/assets_home/css/layouts.css" type="text/css" />
<link rel="stylesheet" href="/assets_home/css/shortcodes.css" type="text/css" />
<link rel="stylesheet" href="/assets_home/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" media="screen" href="/assets_home/css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" type="text/css" href="/assets_home/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />
<link rel="stylesheet" href="/assets_home/css/et-line-font/et-line-font.css">
<link href="/assets_home/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link rel="stylesheet" href="/assets_home/js/masterslider/style/masterslider.css" />

<link rel="stylesheet" type="text/css" href="/assets_home/js/smart-forms/smart-forms.css">



<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="/assets_home/css/colors/lightblue.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/green.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/red.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/violet.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/cyan.css" />-->
<!--<link rel="stylesheet" href="/assets_home/css/colors/mossgreen.css" />-->
