<div class="menu-bg">
    <div class="navbar brown navbar-default yamm">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="/" class="navbar-brand left-padding">
          <img src="/assets_home/images/logo.png" alt="" />
        </a>
        </a>
      </div>
      <div id="navbar-collapse-grid" class="navbar-collapse collapse pull-right">
        <ul class="nav brown navbar-nav">
          <li>
            <a href="/" class="dropdown-toggle active">Home</a>
          </li>
          <li>
            <a href="/about" class="dropdown-toggle">About Us</a>
          </li>
          <li>
            <a href="/menu-cafe" class="dropdown-toggle">Menu</a>
          </li>
          <li>
            <a href="/contact" class="dropdown-toggle">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
