<!doctype html>
<!--[if IE 7 ]>
<html lang="en-gb" class="isie ie7 oldie no-js">
	<![endif]-->
<!--[if IE 8 ]>
	<html lang="en-gb" class="isie ie8 oldie no-js">
		<![endif]-->
<!--[if IE 9 ]>
		<html lang="en-gb" class="isie ie9 no-js">
			<![endif]-->
<!--[if (gt IE 9)|!(IE)]>
			<!-->
<!--
			<![endif]-->
<!--
			<![endif]-->
<html lang="en">
  <head>
    <title>Steamin' Mugs Coffee</title>
    @include('head')
  </head>
  <body>
    <div class="site_wrapper">
      <div id="header8">
        <div class="container">
            @include('menu')
        </div>
      </div>
      <!--end menu-->
      <div class="clearfix"></div>
      <!-- masterslider -->
          @yield('title')
        <!-- slide 1 -->
      <!-- end of masterslider -->
      <div class="clearfix"></div>
      @yield('content1')
      <!--end section-->
      <div class="clearfix"></div>
      @yield('content2')
      <!--end section-->
      <div class="clearfix"></div>
      @yield('content3')
      <div class="clearfix"></div>
      @yield('content4')
      <div class="clearfix"></div>
      @yield('content5')
      <div class="clearfix"></div>
      @yield('content6')
      <!--end section-->
      <div class="clearfix"></div>
       @include('bottom')
       <div class="clearfix"></div>
      <a href="#" class="scrollup brown"></a>
      <!-- end scroll to top of the page-->
    </div>
    <!-- end site wraper -->
     @include('foot')
  </body>
  @yield('js')
</html>
