<!-- ============ JS FILES ============ -->

<script type="text/javascript" src="/assets_home/js/universal/jquery.js"></script>
<script src="/assets_home/js/bootstrap/bootstrap.min.js" type="text/javascript"></script>
<script src="/assets_home/js/masterslider/jquery.easing.min.js"></script>
<script src="/assets_home/js/masterslider/masterslider.min.js"></script>
<script>



$(function(){
   // Add click-event listener
   $(".mobilenav .menu li a").on("click",function(){
      // Remove the current class from all a tags
      $(".mobilenav .menu li a").removeClass("current");
      // Add the current class to the clicked a
      $(this).addClass("current");
   });

});
</script>
<script type="text/javascript">
(function($) {
 "use strict";
	var slider = new MasterSlider();
	// adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	slider.setup('masterslider' , {
		 width:1600,    // slider standard width
		 height:795,   // slider standard height
		 space:0,
		 speed:45,
		 layout:'fullwidth',
		 loop:true,
		 preload:0,
		 autoplay:true,
		 view:"parallaxMask"
	});
})(jQuery);
</script>
<script src="/assets_home/js/menu-cafe/menu.js"></script>
 <script>
 $(".hover").mouseleave(
    function() {
      $(this).removeClass("hover");
}

  );</script>
<script src="/assets_home/js/mainmenu/customeUI.js"></script>
<script src="/assets_home/js/owl-carousel/owl.carousel.js"></script>
<script src="/assets_home/js/owl-carousel/custom.js"></script>
<script src="/assets_home/js/scrolltotop/totop.js"></script>
<script src="/assets_home/js/mainmenu/jquery.sticky.js"></script>
<script src="/assets_home/js/pagescroll/animatescroll.js"></script>
<script src="/assets_home/js/scripts/functions.js" type="text/javascript"></script>

<script type="text/javascript" src="/assets_home/js/smart-forms/jquery.form.min.js"></script>
<script type="text/javascript" src="/assets_home/js/smart-forms/jquery.validate.min.js"></script>
<script type="text/javascript" src="/assets_home/js/smart-forms/additional-methods.min.js"></script>
<script type="text/javascript" src="/assets_home/js/smart-forms/smart-form.js"></script>
<script type="text/javascript" src="/assets_home/js/gmaps/jquery.gmap.js"></script>
<script type="text/javascript" src="/assets_home/js/gmaps/examples.js"></script>

<script src="/assets_home/js/scripts/functions.js" type="text/javascript"></script>


