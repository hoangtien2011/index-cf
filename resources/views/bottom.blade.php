<div id="section-7"></div>
<section class="section-dark sec-padding">
  <div class="container ">

   <div class="row">

   <div class="col-md-4 bmargin">
   <div class="iconbox-small left grayoutline round"><i class="fas fa-map-marker-alt"></i></div>
   <div class="text-box-right more-padding-1">
   <h5 class="text-white roboto-slab uppercase">Địa chỉ</h5>
   <p><i class="fas fa-map-marker-alt"></i> 166/6 Nguyễn Công Trứ, Sơn Trà, Đà Nẵng
   </p>
   </div>
   </div>
   <!--end item-->

   <div class="col-md-4 bmargin">
   <div class="iconbox-small left grayoutline round"><i class="fa fa-coffee"></i></div>
   <div class="text-box-right more-padding-1">
   <h5 class="text-white roboto-slab uppercase">Thời gian làm việc</h5>
   <p>Thứ hai - Chủ nhật <br/>7.00 AM - 11.00 PM</p>

   </div>
   </div>
   <!--end item-->

   <div class="col-md-4 bmargin">
   <div class="iconbox-small left grayoutline round"><i class="fa fa-phone"></i></div>
   <div class="text-box-right more-padding-1">
   <h5 class="text-white roboto-slab uppercase">Thông tin liên lạc</h5>
   <p><i class="fa fa-phone"></i> Phone: 0929 305 691</p>
   </div>
   </div>
   <!--end item-->

  <div class="clearfix"></div>
  <br/>
  <div class="divider-line solid dark-2"></div>
   <br/>
   <div class="col-md-4 col-md-offset-4">
   <ul class=" social-icons-3 brown">
          <li><a href="/"><i class="fa fa-twitter"></i></a></li>
          <li><a href="https://www.facebook.com/hoangviettien2011" target="blank"><i class="fa fa-facebook"></i></a></li>
          <li><a href="/"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="https://www.instagram.com/1102_neit/" target="blank"><i class="fa fa-instagram"></i></a></li>
          <li><a href="https://m.facebook.com/hoangviettien2011" target="blank"><i class="fa fa-android"></i></a></li>
        </ul>
        </div>

   </div>

  </div>
</section>
<!--end section-->
<div class="clearfix"></div>

<section class="section-copyrights sec-moreless-padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12"> <span>Copyright © 2021 <a href="/">Steamin' Mugs Coffee</a></span></div>
    </div>
  </div>
</section>
 <!--end section-->
