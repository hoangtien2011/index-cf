<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

Route::get('/about', function () {
    return view('about');
});
Route::get('/menu-cafe', function () {
    return view('menu-cafe');
});
Route::get('/contact', function () {
    return view('contact');
});

Route::get('/admint', function () {
    return view('SQL.admin');
});
